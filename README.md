# API docs resources

Contains the static resources necessary to generate API documentation, to be
accessed when generating the website.
